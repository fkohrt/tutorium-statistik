# Tests Statistik 2

## Infos

- Sprache: [mermaid](https://github.com/knsv/mermaid)
- erstellt mit dem [Mermaid Live Editor](https://mermaidjs.github.io/mermaid-live-editor/)
- nach PNG exportieren mit Firefox:
  1. Web-Entwickler
  2. Bildschirmgrößen testen
  3. 4200 x 1600 einstellen
  4. Screenshot

## Code

```mermaid
graph TD
    O((Tests)) --> Lage(("Lage(unterschied)"))
    O --> Anp((Anpassung))
    O --> Str(("Streuung(sunterschied)"))
    O --> Zus((Zusammenhang))

    Lage -- "intervallskaliert // MeWe norm.-vert." --> Lage_par[parametrisch]
    Lage -- "ordinalskaliert // Ränge gl.-vert. // (keine Bindungen)" --> Lage_non-par[non-parametrisch]

    subgraph Mittelwerte
        Lage_par --> Lage_par_1[1 SP]
        Lage_par --> Lage_par_2[2 SP]
        Lage_par --> Lage_par_viele[viele SP]

        subgraph " "
            Lage_par_1 --> Lage_par_1_sd-bek[SD bek.]
            Lage_par_1 --> Lage_par_1_sd-unb[SD unb.]

            Lage_par_1_sd-bek --> Lage_par_1_sd-bek_test{z}
            Lage_par_1_sd-unb --> Lage_par_1_sd-unb_test{t}
        end

        Lage_par_2 -. "abh." .-> Lage_par_1
        subgraph " "
            Lage_par_2 -- "unabh." --> Lage_par_2_sd-gl[SD gleich]
            Lage_par_2 -- "unabh." --> Lage_par_2_sd-ver[SD versch.]

            Lage_par_2_sd-gl -- "Var.-Hom." --> Lage_par_2_sd-gl_bek[SD bek.]
            Lage_par_2_sd-gl -- "Var.-Hom." --> Lage_par_2_sd-gl_unb[SD unb.]

            Lage_par_2_sd-ver --> Lage_par_2_sd-ver_bek[SD bek.]
            Lage_par_2_sd-ver --> Lage_par_2_sd-ver_unb[SD unb.]

            Lage_par_2_sd-gl_bek --> Lage_par_2_sd-gl_bek_test{z}
            Lage_par_2_sd-gl_unb --> Lage_par_2_sd-gl_bek_ver[H_1: versch.]
            Lage_par_2_sd-gl_unb --> Lage_par_2_sd-gl_bek_aequiv[H_1: äquiv.]

            Lage_par_2_sd-ver_bek --> Lage_par_2_sd-ver_bek_test{z}
            Lage_par_2_sd-ver_unb --> Lage_par_2_sd-ver_unb_test{Welch}

            Lage_par_2_sd-gl_bek_ver --> Lage_par_2_sd-gl_bek_ver_test{t}
            Lage_par_2_sd-gl_bek_aequiv --> Lage_par_2_sd-gl_bek_aequiv_test1{Krücke}
            Lage_par_2_sd-gl_bek_aequiv --> Lage_par_2_sd-gl_bek_aequiv_test2{Äquiv.}
        end

        subgraph " "
            Lage_par_viele -- "unabh." --> Lage_par_viele_1[1 Fakt.]
            Lage_par_viele -- "unabh." --> Lage_par_viele_2[2 Fakt.]

            Lage_par_viele_1 --> Lage_par_viele_1_sd-gl[SD gleich]
            Lage_par_viele_2 --> Lage_par_viele_2_sd-gl[SD gleich]

            Lage_par_viele_1_sd-gl -- "Var.-Hom." --> Lage_par_viele_1_sd-gl_unb[SD unb.]
            Lage_par_viele_2_sd-gl -- "Var.-Hom." --> Lage_par_viele_2_sd-gl_unb[SD unb.]

            Lage_par_viele_1_sd-gl_unb --> Lage_par_viele_1_sd-gl_unb_test{einfakt. // ANOVA}
            Lage_par_viele_2_sd-gl_unb --> Lage_par_viele_2_sd-gl_unb_test{zweifakt. // ANOVA}
        end
    end

        subgraph Ränge
            Lage_non-par --> Lage_non-par_2[2 SP]

            Lage_non-par_2 -- "abh." --> Lage_non-par_2_abh_kB[k. Bind.]
            Lage_non-par_2 -- "abh." --> Lage_non-par_2_abh_B[Bind.]

            Lage_non-par_2 -- "unabh." --> Lage_non-par_2_unabh_kB[k. Bind.]
            Lage_non-par_2 -- "unabh." --> Lage_non-par_2_unabh_B[Bind.]

            Lage_non-par_2_abh_kB --> Lage_non-par_2_abh_kB_kl[kl.]
            Lage_non-par_2_abh_kB --> Lage_non-par_2_abh_kB_gr[gr.]
            Lage_non-par_2_abh_B --> Lage_non-par_2_abh_B_test{NV-App.}

            Lage_non-par_2_unabh_kB --> Lage_non-par_2_unabh_kB_kl[kl.]
            Lage_non-par_2_unabh_kB --> Lage_non-par_2_unabh_kB_gr[gr.]
            Lage_non-par_2_unabh_B --> Lage_non-par_2_unabh_B_test{NV-App.}

            Lage_non-par_2_abh_kB_kl --> Lage_non-par_2_abh_kB_kl_test{Wilc.}
            Lage_non-par_2_abh_kB_gr --> Lage_non-par_2_abh_kB_gr_test{NV-App.}

            Lage_non-par_2_unabh_kB_kl --> Lage_non-par_2_unabh_kB_kl_test{U}
            Lage_non-par_2_unabh_kB_gr --> Lage_non-par_2_unabh_kB_gr_test{NV-App.}
        end

    subgraph Häufigkeiten
        Anp -- "nominalskaliert" --> di[dich.]
        Anp -- "nominalskaliert" --> poly[polych.]

        di --> di_1[1 Merkm.]
        di --> di_2[2 Merkm.]

        poly --> poly_1[1 Merkm.]
        poly --> poly_2[2 Merkm.]

        di_1 -- "Beob. unabh." --> di_1_0-mwh[0 MWH]
        di_1 --> di_1_1-mwh[1 MWH]
        di_1 --> di_1_viele-mwh[viele MWH]

        di_2 -- "Beob. unabh." --> di_2_0-mwh[0 MWH]

        poly_1 -- "Beob. unabh." --> poly_1_0-mwh[0 MWH]
        poly_2 -- "Beob. unabh." --> poly_2_0-mwh[0 MWH]

        di_1_0-mwh --> di_1_0-mwh_test{Chi^2}
        di_1_1-mwh --> di_1_1-mwh_test{McNemar}
        di_1_viele-mwh --> di_1_viele-mwh_test{Cochran}

        di_2_0-mwh --> di_2_0-mwh_bek["Kat.-Wsk. bek."]
        di_2_0-mwh --> di_2_0-mwh_unb["Kat.-Wsk. unb."]

        poly_1_0-mwh --> poly_1_0-mwh_test{Chi^2}
        poly_2_0-mwh --> poly_2_0-mwh_test{r x c}

        di_2_0-mwh_bek --> di_2_0-mwh_bek_test{"4-F.-Anp."}
        di_2_0-mwh_unb --> di_2_0-mwh_unb_test{"4-F.-Chi^2-Unabh."}
    end

    subgraph " "
        Str -- "intervallskaliert" --> Str_par[parametrisch]

        Str_par --> Str_par_1[1 SP]
        Str_par --> Str_par_2[2 SP]

        Str_par_1 --> Str_par_1_test{Chi^2}
        Str_par_2 --> Str_par_2_test{F}
    end
```
