# Tutorium Statistik

## Statistik Allgemein

- [Zentrale Statistik-Erkenntnisse](https://fkohrt.gitlab.io/tutorium-statistik/Zentrale_Statistik-Erkenntnisse.html) ([PDF](https://fkohrt.gitlab.io/tutorium-statistik/Zentrale_Statistik-Erkenntnisse.pdf), [Rmd](https://fkohrt.gitlab.io/tutorium-statistik/Zentrale_Statistik-Erkenntnisse.Rmd))

## Statistik 1

- [Zusammenfassung](https://fkohrt.gitlab.io/tutorium-statistik/Zusammenfassung_Statistik-1.html) ([PDF](https://fkohrt.gitlab.io/tutorium-statistik/Zusammenfassung_Statistik-1.pdf), [Rmd](https://fkohrt.gitlab.io/tutorium-statistik/Zusammenfassung_Statistik-1.Rmd))

## Statistik 2

- [Zusammenfassung](https://fkohrt.gitlab.io/tutorium-statistik/Zusammenfassung_Statistik-2.html) ([PDF](https://fkohrt.gitlab.io/tutorium-statistik/Zusammenfassung_Statistik-2.pdf), [Rmd](https://fkohrt.gitlab.io/tutorium-statistik/Zusammenfassung_Statistik-2.Rmd))
- [Tests](https://fkohrt.gitlab.io/tutorium-statistik/Tests_Statistik-2.png) ([mermaid](Tests_Statistik-2.md))
- [Helferlein](https://fkohrt.gitlab.io/tutorium-statistik/Helferlein_Statistik-2.ods)

## Lizenz

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/" class="img"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
